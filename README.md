# Projet ASAC / AP

## Week 1 (25-31 Jan)

- We assisted to the class presented by the professor Donsez Didier.
- We contacted the students from the other groups (IESE/MAT) whom we will be working with.

## Week 2 (01-07 Feb)

- We updated the Wiki Air page and added the missing information about our project as well as added the README.md file to the repository.
- We read the information on the wiki page of the project.

## Week 3 (08-14 Feb)

- We read the documentation of some of the tools we will be using (ex: mqtt, influxdb).
- We read through the main function that's used to create the paquets that get sent. 
- We looked into the flow charts in Node-Red to better understand the functioning of the system.

## Week 4 (22-28 Feb)

- We tried to deploy the Docker fot the MQTT flux. 
- We started preparing files for the mid-project presentation

## Week 5 (1-7 March)

During the Docker configuration we modified the Dockerfile with  
``` FROM nodered/node-red ```
instead of ``` FROM nodered/node-red-docker ```.
We want to use the latest version of the nodered docker. 

## Week 6 (8-14 March)

- Mid-project presentation
- Brainstorming about the mobile application. We disscussed the possibility of using JHipster. 


## Week 7 (15-21 March)

- Read the Jhipster documentation and generating the first test app for the Greenhouse
- Construction of a draft for the entities diagramm for the app
- Discussion with our teacher to clarify up some points on docker and nodeRed 
- Set a new goal : reverse engineering of the last year's application
- Modified some Nodes in NodeRed 


## Week 8 (22-28 March)

- reverse engineering of last year's app features. 
- creation of the JDL file and fixing some bugs in it. 
- Finally fixing InfluxDB databases by using another image instead of the influxdb/influxdb image that was in the docker-compose file. The image we ended up using is tutum/influxdb.
- Sucessfully generating a Jhipster test app from a mock jdl
- Problems with generating the real app with the proper jdl file 
- Next goals : fix the Jhipster generation from the jdl file 
- Deploying all the applications on the docker and making small changes that we displayed on Grafana.

## Week 9 (29 March - 04 April)

-  Finally fixing the jdl file 
- Generating our JHipster application from this jdl
- Tried to change some features in the app
- Changed the welcome page of the application 
- Everyone could deploy their own version of the Docker.